from sqlalchemy import Column, ForeignKey, String, Integer, Text, Table
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

Base = declarative_base()

product_color = Table('product_color', Base.metadata,
                      Column('product_id', Integer, ForeignKey('product.id')),
                      Column('color_id', Integer, ForeignKey('color.id')))

product_size = Table('product_size', Base.metadata,
                     Column('product_id', Integer, ForeignKey('product.id')),
                     Column('size_id', Integer, ForeignKey('size.id')))

product_category = Table('product_category', Base.metadata,
                         Column('product_id', Integer, ForeignKey('product.id')),
                         Column('category_id', Integer, ForeignKey('category.id')))

product_subcategory = Table('product_subcategory', Base.metadata,
                            Column('product_id', Integer, ForeignKey('product.id')),
                            Column('subcategory_id', Integer, ForeignKey('subcategory.id')))


class AttrMixin(object):
    id = Column(Integer, primary_key=True)
    name = Column(String(250), nullable=False)


class Category(Base):
    __tablename__ = 'category'
    id = Column(Integer, primary_key=True)
    name = Column(String(250), nullable=False, unique=True)

    subcategories = relationship('Subcategory', backref='category')
    original_url = Column(Text, nullable=False)


class Subcategory(Base):
    __tablename__ = 'subcategory'
    id = Column(Integer, primary_key=True)
    name = Column(String(250), nullable=False)

    category_id = Column(Integer, ForeignKey('category.id'))
    original_url = Column(Text, nullable=False)


class Color(Base, AttrMixin):
    __tablename__ = 'color'


class Size(Base, AttrMixin):
    __tablename__ = 'size'


class Product(Base):
    __tablename__ = 'product'

    id = Column(Integer, primary_key=True)
    name = Column(String(250), nullable=False)
    details = Column(String(250), nullable=True)
    price = Column(Integer, nullable=False)

    categories = relationship(Category, backref='products', secondary=product_category)
    subcategories = relationship(Subcategory, backref='products', secondary=product_subcategory)
    colors = relationship("Color", secondary=product_color)
    sizes = relationship("Size", secondary=product_size)

    original_url = Column(Text, nullable=False)

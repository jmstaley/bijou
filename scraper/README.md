The scraper consists of two spiders, one to get categories another to get products.
Also included in this module is the database definitions.  This is handled using 
SQLAlchemy and alembic for migrations.


I decided to store the following product information:

 * name
 * details
 * price
 * categories
 * subcategories
 * sizes
 * colors
 * original url

These seem to me to be the bare minimum pieces of data that would be needed to 
start selling elsewhere. The original url maybe useful if there are any queries 
about the data, also for record keeping or further scraping in the future.

It is missing availability but that changes and it might be better to use original url
to scrape that on the fly.  Also images would be nice to have.  

Install
=======

Suggested to use a virtualenv.

pip install --upgrade pip
sudo apt-get install python-dev python-pip libxml2-dev libxslt1-dev zlib1g-dev libffi-dev libssl-dev
pip install -r requirements.txt

Setup
=====
The only setup is to set the database uri.  This needs to be set in two places, `alembic.ini` and `data/session.py`

Run
===
To get the database initialized:

```
alembic upgrade head
```

Run the scrapers:

```
scrapy crawl categories
scrapy crawl product
```

You should now have a database populated.

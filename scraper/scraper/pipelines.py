# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

from scrapy.exceptions import DropItem

from data.models import (
    Category,
    Color,
    Product,
    Size,
    Subcategory
)
from data.session import DBSession

session = DBSession()


class CategoryPipeline(object):
    def process_item(self, item, spider):
        if spider.name != 'categories':
            return item

        name = item['name'].strip().lower()
        cat = session.query(Category).filter_by(name=name).first()
        if not cat:
            cat = Category()
            cat.name = name
            cat.original_url = item['link'].strip()

        for subcat in item.get('subcategories', []):
            name = subcat['name'].strip().lower()
            link = subcat['link'].strip()
            sub = session.query(Subcategory).filter(Subcategory.category.has(name=cat.name)).first()

            if not sub or sub.category.name != cat.name:
                sub = Subcategory()
                sub.name = name
                sub.original_url = link
                cat.subcategories.append(sub)

        session.add(cat)
        session.commit()
        return item


class ProductPipeline(object):
    def __init__(self):
        self.seen = set()

    def clean_price(self, text):
        return int(float(text.encode('ascii', errors='ignore').strip('\n')) * 100)

    def process_item(self, item, spider):
        if spider.name != 'product':
            return item

        name = item['name'].strip().lower()
        if name in self.seen:
            raise DropItem('Duplicate item: %s' % item)
        else:
            self.seen.add(name)

        prod = session.query(Product).filter_by(name=name).first()
        if not prod:
            prod = Product()
            prod.name = name
            prod.price = self.clean_price(item['price'])
            prod.details = '|'.join(item['details'])
            prod.original_url = item['link']

        for item_size in item['sizes']:
            name = item_size.strip().lower()
            size = session.query(Size).filter_by(name=name).first()
            if not size:
                size = Size()
                size.name = name
            prod.sizes.append(size)

        for item_col in item['colors']:
            name = item_col.lower().strip()
            color = session.query(Color).filter_by(name=name).first()
            if not color:
                color = Color()
                color.name = name
            prod.colors.append(color)

        cat_name = item['cat'].lower().strip()

        cat = session.query(Category).filter_by(name=cat_name).first()
        prod.categories.append(cat)

        if item['subcat']:
            sub_name = item['subcat'].lower().strip()
            sub = session.query(Subcategory).filter_by(name=sub_name).first()
            prod.subcategories.append(sub)

        session.add(prod)
        session.commit()
        return item

import scrapy
import urlparse


class ProductScraper(scrapy.Spider):
    name = 'product'
    custom_settings = {
        'ITEM_PIPLINES': {
            'scraper.pipelines.ProductPipeline': 300,
        }
    }
    start_urls = ['http://www.farah.co.uk/clothing/',
                  'http://www.farah.co.uk/accessories/',
                  'http://www.farah.co.uk/footwear/',
                  'http://www.farah.co.uk/suits/',
                  'http://www.farah.co.uk/classic/']

    def parse_product(self, response):
        name = response.css('h1.productname::text').extract_first()
        price = response.xpath('//div[@class="price"]').css('div.salesprice::text').extract_first()

        swatches = response.css('div.color').css('ul.swatchesdisplay').css('li')
        colors = []
        for swatch in swatches:
            color = swatch.xpath('./a[@class="swatchanchor"]/text()').extract_first()
            colors.append(color)

        swatches = response.css('div.size').css('ul.swatchesdisplay').css('li')
        sizes = []
        for swatch in swatches:
            size = swatch.xpath('./a[@class="swatchanchor"]/text()').extract_first()
            sizes.append(size)

        detail_list = response.css('div#pdpTab2').css('li::text')
        details = []
        for det in detail_list:
            detail = det.extract()
            details.append(detail)

        # use breadcrumbs to get categories
        breadcrumbs = response.css('div#breadcrumb').css('a::text')
        if len(breadcrumbs) == 4:
            subcat = breadcrumbs[3].extract()
            cat = breadcrumbs[2].extract()
        elif len(breadcrumbs) == 3:
            subcat = breadcrumbs[2].extract()
            cat = breadcrumbs[1].extract()
        else:
            subcat = None
            cat = breadcrumbs[1].extract()

        # get product url
        clean_url = urlparse.urljoin(response.url, urlparse.urlparse(response.url).path)

        yield {'name': name.lower(),
               'price': price,
               'colors': colors,
               'sizes': sizes,
               'details': details,
               'link': clean_url,
               'cat': cat,
               'subcat': subcat}

    def parse_list(self, response):
        items = response.css('div.producttile')

        # loop through each product title
        for item in items:
            link = item.css('div.name').css('a::attr(href)').extract_first()
            product = response.urljoin(link)

            yield scrapy.Request(product, self.parse_product)

        # is there pagination? if so follow
        current_page = response.css('li.currentpage::text').extract_first()
        if current_page:
            next_page = int(current_page.strip('\n')) + 1

            next_link = response.css('a.page-%s::attr(href)' % next_page).extract_first()
            if next_link:
                next_list = response.urljoin(next_link)
                yield scrapy.Request(next_list, self.parse_list)

    def parse_category(self, response):
        refine = response.css('ul#category-level-2')

        # go deeper! on to level 2
        for link in refine.css('li').css('a.refineLink'):
            link_url = link.css('a::attr(href)').extract_first()
            category = response.urljoin(link_url)
            # these should be product list pages now
            yield scrapy.Request(category, self.parse_list)

    def parse(self, response):
        refine = response.css('ul.refinementcategory')

        # are there subcategories if so follow them
        if refine:
            for link in refine.css('a.refineLink'):
                link_url = link.css('a::attr(href)').extract_first()
                category = response.urljoin(link_url)

                yield scrapy.Request(category, self.parse_category)
        else:
            # no subcatgories then we must be on a list page
            url = response.urljoin(response.url)
            yield scrapy.Request(url, self.parse_list)

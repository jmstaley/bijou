import scrapy


class CategoryScraper(scrapy.Spider):
    name = 'categories'
    custom_settings = {
        'ITEM_PIPLINES': {
            'scraper.pipelines.CategoryPipeline': 300,
        }
    }
    start_urls = ['http://www.farah.co.uk/', ]

    def parse(self, response):
        # top level menu items
        menu = ['farahc', 'faraha', 'farahfootwear', 'farahsuits', 'farahclassic']

        for item in menu:
            # loop through the top level menu items
            for data in response.xpath("//li[@id='%s']" % item):
                submenu = data.css('div.subMenuContent').css('ul.%s' % item).css('li')

                if data.css('a::text').extract_first() == 'Classic':
                    cat = data.css('a::text').extract_first()
                    link = data.xpath('./a').css('a::attr(href)').extract_first()
                    subs = []
                    for x in submenu:
                        sub_cat = x.css('a::text').extract_first()
                        sub_link = x.xpath('./a').css('a::attr(href)').extract_first()
                        if sub_cat and link:
                            subs.append({'name': sub_cat, 'link': sub_link})
                    yield {'name': cat,
                           'link': link,
                           'subcategories': subs}
                else:
                    for x in submenu:
                        # if there are subcategories attach them to the category
                        subs = x.css('div.subcategories')
                        if subs:
                            cat = x.css('a::text').extract_first()
                            link = x.xpath('./a').css('a::attr(href)').extract_first()

                            sub_cats = []
                            for sub in x.css('div.subcategories'):
                                sub_cat = sub.css('a::text').extract_first()
                                sub_link = sub.xpath('./a').css('a::attr(href)').extract_first()
                                if sub_cat and sub_link:
                                    sub_cats.append({'name': sub_cat, 'link': sub_link})
                            yield {'name': cat,
                                   'link': link,
                                   'subcategories': sub_cats}
                        else:
                            cat = data.css('a::text').extract_first()
                            link = data.xpath('./a').css('a::attr(href)').extract_first()

                            if cat != 'Clothing':
                                yield {'name': cat,
                                       'link': link}

The micro service is a simple Flask application.  It uses SQLALchemy reflection
to build the models from the existing database.

The service exposes four endpoints

  * `/category/`
  * `/category/<category id>/`
  * `/product/` - accepts querystring category and subcategory, the ids of those
  * `/product/<product id>/`

Install
=======
Suggested to use a virtualenv:

pip install -r requirements.txt

Setup
=====
Set the database uri in `database.py`

Run
===
`python run.py`

This will start the development server on http://127.0.0.1:5000/

You can configure the host and port using `--host` and `--port` arguments.

from database import app
from views import ProductView, CategoryView

product_view = ProductView.as_view('product_view')
app.add_url_rule('/product/',
                 defaults={'product_id': None},
                 view_func=product_view,
                 methods=['GET', ])

app.add_url_rule('/product/<int:product_id>/',
                 view_func=product_view,
                 methods=['GET', ])

category_view = CategoryView.as_view('category_view')
app.add_url_rule('/category/',
                 defaults={'category_id': None},
                 view_func=category_view,
                 methods=['GET', ])
app.add_url_rule('/category/<int:category_id>/',
                 view_func=category_view,
                 methods=['GET', ])

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Run development server')
    parser.add_argument('--host', dest='host', help='host')
    parser.add_argument('--port', dest='port', help='port')
    args = parser.parse_args()
    app.run(host=args.host, port=args.port)

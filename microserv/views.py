from flask import jsonify, request
from flask.views import MethodView

from database import db, Product, Category, Subcategory
from serializers import serialize_product, serialize_category


class ProductView(MethodView):
    def get(self, product_id):

        if product_id:
            product = db.session.query(Product).get(product_id)

            return jsonify(serialize_product(product))

        else:
            category = request.args.get('category')
            sub = request.args.get('sub')

            products = []
            query = db.session.query(Product)
            if category:
                query = query.join(Product.category_collection).filter(Category.id == category)
            if sub:
                query = query.join(Product.subcategory_collection).filter(Subcategory.id == sub)
            for prod in query.all():
                products.append(serialize_product(prod))

            return jsonify({'items': products})


class CategoryView(MethodView):
    def get(self, category_id):
        if category_id:
            category = db.session.query(Category).get(category_id)

            return jsonify(serialize_category(category))

        else:
            categories = []
            for cat in db.session.query(Category).all():
                categories.append(serialize_category(cat))

            return jsonify({'items': categories})

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import MetaData
from sqlalchemy.ext.automap import automap_base

# need to set up this before everything else so that we can reflect properly
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////home/vagrant/farah/scraper/product_db.sqlite'

db = SQLAlchemy(app)

metadata = MetaData()
metadata.reflect(db.engine)
Base = automap_base(metadata=metadata)
Base.prepare()

Category = Base.classes.category
Subcategory = Base.classes.subcategory
Color = Base.classes.color
Size = Base.classes.size
Product = Base.classes.product

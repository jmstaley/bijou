def serialize_category(cat, subs=True):
    s_cat = {'id': cat.id,
             'name': cat.name}
    if subs:
        s_cat['subcategories'] = [serialize_subcategory(item) for item in cat.subcategory_collection]
    return s_cat


def serialize_subcategory(cat):
    return {'id': cat.id,
            'name': cat.name}


def serialize_color(color):
    return {'id': color.id,
            'name': color.name}


def serialize_size(size):
    return {'id': size.id,
            'name': size.name}


def serialize_product(product):
    return {'id': product.id,
            'name': product.name,
            'details': product.details,
            'price': product.price,
            'sizes': [serialize_size(item) for item in product.size_collection],
            'colors': [serialize_color(item) for item in product.color_collection],
            'category': [serialize_category(item, subs=False) for item in product.category_collection],
            'subcategory': [serialize_subcategory(item) for item in product.subcategory_collection]}

The two parts of the assignment are in here.

The scraper is in `scraper/` and the microservice is in `microserv`

See README files in the respective folders for more details

Scraper
=======
The scraper was built using scrapy.  I don't have experience writing or deploying scrapers
and scrapy seemed to be fairly easy to get going with.  Using SQLAlchemy to store the results
into a sqlite database.

MicroService
============
Built using Flask.  It uses SQLAlchemy reflection to build the models from the database created
by the scraper.
